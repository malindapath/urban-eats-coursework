var searchResturantList = [
    {restaurantID:0,thumbnailImage:"../themes/images/eshankaHomeThumbnail.png", resturantName:"Eshanka Home", restaurantLocation: "Colombo 6  |  25-33 min", rating: 5, isFavourite:false},
    {restaurantID:1,thumbnailImage:"../themes/images/homepaanThumbnail.png", resturantName:"Home Paan", restaurantLocation: "Bellanvila  |  50-60 min", rating: 5, isFavourite:false},
    {restaurantID:2,thumbnailImage:"../themes/images/homelyFoodThumbnail.png", resturantName:"Homely Food", restaurantLocation: "Colombo 9  |  70-80 min", rating: 3, isFavourite:false},
    {restaurantID:3,thumbnailImage:"../themes/images/homeskitchenThumbnail.png", resturantName:"Home's Kitchen", restaurantLocation: "Nugegoda  |  10-20 min", rating: 4, isFavourite:false},
    {restaurantID:4,thumbnailImage:"../themes/images/serendibhomekitchenThumbnail.png", resturantName:"Serendib Home Kitchen", restaurantLocation: "Colombo 3  |  30-40 min", rating: 5, isFavourite:false }  
]

$(document).on("pageshow", function (event) {
    localStorage.setItem("searchList", JSON.stringify(searchResturantList));
    loadCategoryRestaurantList(searchResturantList);
});

function loadCategoryRestaurantList(retrievedList) {
    var list = document.getElementById('searchList');
    var stringList = "";
    for (var i = 0;i < retrievedList.length;i++) {
        var ratingImage;
        if(retrievedList[i].rating == 5){
            ratingImage = "../themes/images/5starRating.png"
        }else{
            ratingImage = "../themes/images/4starRating.png"
        }
        var newString;
        if(retrievedList[i].restaurantID == 0){
            newString = `<div id = "restaurantCard" class = "eshankHomeCard"><div id="restaurantCardGrid"><div id = "restaurantThumbnailContainer"><img src="${retrievedList[i].thumbnailImage}" alt="Restaurant Thumbnail" id = "restaurantThumbnail"></div><div id = "restaurantCardData"><p id="restaurantCardMainText">${retrievedList[i].resturantName}</p><p id="restaurantCardSubText">${retrievedList[i].restaurantLocation}</p><img src="${ratingImage}" alt="5 Star Rating" id = "ratingStars"></div><div id = restaurantCardGridHeartIconContainer><img src="${retrievedList[i].isFavourite == true?"../themes/images/activatedHeartIcon.png":"../themes/images/deactivatedHeartIcon.png"}" onclick = "onFavouriteIconClicked(${retrievedList[i].restaurantID});" alt="Favourite Icon" id = "heartIcon"></div></div></div>`
        }else{
            newString = `<div id = "restaurantCard"><div id="restaurantCardGrid"><div id = "restaurantThumbnailContainer"><img src="${retrievedList[i].thumbnailImage}" alt="Restaurant Thumbnail" id = "restaurantThumbnail"></div><div id = "restaurantCardData"><p id="restaurantCardMainText">${retrievedList[i].resturantName}</p><p id="restaurantCardSubText">${retrievedList[i].restaurantLocation}</p><img src="${ratingImage}" alt="5 Star Rating" id = "ratingStars"></div><div id = restaurantCardGridHeartIconContainer><img src="${retrievedList[i].isFavourite == true?"../themes/images/activatedHeartIcon.png":"../themes/images/deactivatedHeartIcon.png"}" onclick = "onFavouriteIconClicked(${retrievedList[i].restaurantID});" alt="Favourite Icon" id = "heartIcon"></div></div></div>`
        }
        
        stringList = stringList + newString;
    }
    list.innerHTML = stringList;
};

$(document).on('click', '.eshankHomeCard', function(e){
    location.href = "restaurant.html"
});


