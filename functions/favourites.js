
var categoryRestaurantList = [
    {restaurantID:0,thumbnailImage:"../themes/images/eshankaHomeThumbnail.png", resturantName:"Eshanka Home", restaurantLocation: "Colombo 6  |  25-33 min", rating: 5, isFavourite:false, deliveryTime: 25, resturantPlace:"Colombo 6"},
    {restaurantID:1,thumbnailImage:"../themes/images/nelumKoleThumbnail.png", resturantName:"Nelum Kole - Kohuwala", restaurantLocation: "Kohuwala  |  25-33 min", rating: 4, isFavourite:false,deliveryTime: 25, resturantPlace:"Kohuwala"},
    {restaurantID:2,thumbnailImage:"../themes/images/thaiExpressThumbnail.png", resturantName:"Thai Express", restaurantLocation: "Colombo 4  |  30-35 min", rating: 5, isFavourite:false,deliveryTime: 30, resturantPlace:"Colombo 4"},
    {restaurantID:3,thumbnailImage:"../themes/images/biteMeThumbnail.png", resturantName:"Bite Me", restaurantLocation: "Kotte  |  45-55 min", rating: 4, isFavourite:false, deliveryTime: 40, resturantPlace:"Kotte"},
    {restaurantID:4,thumbnailImage:"../themes/images/shanayaFoodsThumbnail.png", resturantName:"Shanaya Foods", restaurantLocation: "Borella  |  60-70 min", rating: 4, isFavourite:false, deliveryTime: 60, resturantPlace:"Borella"}
]

var favCuisinesList = [
    {cuisineID:0,thumbnailImage:"../themes/images/chickSoup.png", cuisineName:"Chicken Soup", price: "LKR 720.00", resturantName: "Homely Food", isFavourite:true},
    {cuisineID:1,thumbnailImage:"../themes/images/vegSoup.png", cuisineName:"Vegetable Soup", price: "LKR 620.00", resturantName: "Eshanka Home", isFavourite:true},
]


$(document).on('click', '.stars5Btn', function(){ 
   
    var id_filter = [0,1,2,3,4];
    var filtered = categoryRestaurantList.filter(function(item){
        return id_filter.indexOf(item.restaurantID) !==-1 && item.rating==5;
    })
    
    loadCategoryRestaurantList(filtered)
});

$(document).on('click', '.25minBtn', function(){ 
    
    var id_filter = [0,1,2,3,4];
    var filtered = categoryRestaurantList.filter(function(item){
        return id_filter.indexOf(item.restaurantID) !==-1 && item.deliveryTime==25;
    })
    loadCategoryRestaurantList(filtered)
});

$(document).on('click', '.col6Btn', function(){ 
    
    var id_filter = [0,1,2,3,4];
    var filtered = categoryRestaurantList.filter(function(item){
        return id_filter.indexOf(item.restaurantID) !==-1 && item.resturantPlace==="Colombo 6";
    })
    loadCategoryRestaurantList(filtered)
});

$(document).on('click', '.stars3Btn', function(){ 
    
    var id_filter = [0,1,2,3,4];
    var filtered = categoryRestaurantList.filter(function(item){
        return id_filter.indexOf(item.restaurantID) !==-1 && item.rating==3;
    })
    loadCategoryRestaurantList(filtered)
});

$(document).on('click', '.stars4Btn', function(){ 
    
    var id_filter = [0,1,2,3,4];
    var filtered = categoryRestaurantList.filter(function(item){
        return id_filter.indexOf(item.restaurantID) !==-1 && item.rating==4;
    })
    loadCategoryRestaurantList(filtered)
});

$(document).on('click', '.10minBtn', function(){ 
    
    var id_filter = [0,1,2,3,4];
    var filtered = categoryRestaurantList.filter(function(item){
        return id_filter.indexOf(item.restaurantID) !==-1 && item.deliveryTime==10;
    })
    loadCategoryRestaurantList(filtered)
});

$(document).on('click', '.applyBtn', function(){
    var val1 = $('input[name=rating]:checked').val();
    var val2 = $('input[name=duration]:checked').val();

    var id_filter1 = [0,1,2,3,4];
    var filtered1 = categoryRestaurantList.filter(function(item){
        if (val1 == 2){
            return id_filter1.indexOf(item.restaurantID) !==-1 && item.rating>val1 && item.deliveryTime<val2;
        }
        else{
            return id_filter1.indexOf(item.restaurantID) !==-1 && item.rating==val1 && item.deliveryTime<val2;
        }
        
    })
    loadCategoryRestaurantList(filtered1)
});

// $(document).on('click', '.applyBtn', function(){
//     var val1 = $('input[name=rating]:checked').val();
//     var val2 = $('input[name=duration]:checked').val();

//     var id_filter1 = [0,1,2,3,4];
//     var filtered1 = categoryRestaurantList.filter(function(item){
//         if (val1 == 2){
//             return id_filter1.indexOf(item.restaurantID) !==-1 && item.rating>val1;
//         }
//         else{
//             return id_filter1.indexOf(item.restaurantID) !==-1 && item.rating==val1;
//         }
        
//     })
//     loadCategoryRestaurantList(filtered1)
// });

// $(document).on('click', '.applyBtn', function(){
//     var val1 = $('input[name=rating]:checked').val();
//     var val2 = $('input[name=duration]:checked').val();

//     var id_filter1 = [0,1,2,3,4];
//     var filtered1 = categoryRestaurantList.filter(function(item){
//             return id_filter1.indexOf(item.restaurantID) !==-1 &&  item.deliveryTime<val2;
//         // return id_filter1.indexOf(item.restaurantID) !==-1 && item.rating==val1 && item.deliveryTime===val2;
//     })
//     loadCategoryRestaurantList(filtered1)
// });

$(document).on('click','.applyBtn',function(){
    $('.openResturantFilterPopup').popup();
    $('.openResturantFilterPopup').popup('close');
});

$(document).on('click', '.filterCancelBtn', function(){ 
    categoryRestaurantList = categoryRestaurantList;
    loadCategoryRestaurantList(categoryRestaurantList)
});

$(document).on('click','.filterCancelBtn',function(){
    $('.openResturantFilterPopup').popup();
    $('.openResturantFilterPopup').popup('close');
});

$(document).on("pageshow", function (event) {
    var page = event.target.getAttribute('key');
    if ( page == "restaurantListPage") {
        var storedRestaurantList = JSON.parse(localStorage.getItem("restaurantList"));
        if(storedRestaurantList == null){
            localStorage.setItem("restaurantList", JSON.stringify(categoryRestaurantList));
            loadCategoryRestaurantList(categoryRestaurantList);
        }else{
            loadCategoryRestaurantList(storedRestaurantList);
        }
    }
    if ( page == "favResPage") {
        var storedRestaurantList = JSON.parse(localStorage.getItem("restaurantList"));
        if(storedRestaurantList == null){
            localStorage.setItem("restaurantList", JSON.stringify(categoryRestaurantList));
            loadFavouriteRestaurantList(categoryRestaurantList);
        }else{
            loadFavouriteRestaurantList(storedRestaurantList);
        }
    }
});

function loadCategoryRestaurantList(retrievedList) {
    var list = document.getElementById('restaurantList');
    var stringList = "";
    for (var i = 0;i < retrievedList.length;i++) {
        var ratingImage;
        if(retrievedList[i].rating == 5){
            ratingImage = "../themes/images/5starRating.png"
        }else{
            ratingImage = "../themes/images/4starRating.png"
        }
        var newString;
        if(retrievedList[i].restaurantID == 0){
            newString = `<div id = "restaurantCard" class = "eshankHomeCard"><div id="restaurantCardGrid"><div id = "restaurantThumbnailContainer"><img src="${retrievedList[i].thumbnailImage}" alt="Restaurant Thumbnail" id = "restaurantThumbnail"></div><div id = "restaurantCardData"><p id="restaurantCardMainText">${retrievedList[i].resturantName}</p><p id="restaurantCardSubText">${retrievedList[i].restaurantLocation}</p><img src="${ratingImage}" alt="5 Star Rating" id = "ratingStars"></div><div id = restaurantCardGridHeartIconContainer><img src="${retrievedList[i].isFavourite == true?"../themes/images/activatedHeartIcon.png":"../themes/images/deactivatedHeartIcon.png"}" onclick = "onFavouriteIconClicked(${retrievedList[i].restaurantID});" alt="Favourite Icon" id = "heartIcon"></div></div></div>`
        }else{
            newString = `<div id = "restaurantCard"><div id="restaurantCardGrid"><div id = "restaurantThumbnailContainer"><img src="${retrievedList[i].thumbnailImage}" alt="Restaurant Thumbnail" id = "restaurantThumbnail"></div><div id = "restaurantCardData"><p id="restaurantCardMainText">${retrievedList[i].resturantName}</p><p id="restaurantCardSubText">${retrievedList[i].restaurantLocation}</p><img src="${ratingImage}" alt="5 Star Rating" id = "ratingStars"></div><div id = restaurantCardGridHeartIconContainer><img src="${retrievedList[i].isFavourite == true?"../themes/images/activatedHeartIcon.png":"../themes/images/deactivatedHeartIcon.png"}" onclick = "onFavouriteIconClicked(${retrievedList[i].restaurantID});" alt="Favourite Icon" id = "heartIcon"></div></div></div>`
        }
        
        stringList = stringList + newString;
    }
    list.innerHTML = stringList;
};

$(document).on('click', '.eshankHomeCard', function(e){
    if($(e.target).is("img#heartIcon") == false){
        location.href = "restaurant.html"
    } 
});

function loadFavouriteRestaurantList(retrievedList) {
    var restaurantListTabBar = document.getElementsByClassName("restaurantsTabBar")[0]
    restaurantListTabBar.innerHTML = '<div id = "yellowLineTabBar"></div>'
    var list = document.getElementById('favouriteRestaurantList');
    var stringList = "";
    for (var i = 0;i < retrievedList.length;i++) {
        if(retrievedList[i].isFavourite){
        var ratingImage;
        if(retrievedList[i].rating == 5){
            ratingImage = "../themes/images/5starRating.png"
        }else{
            ratingImage = "../themes/images/4starRating.png"
        }
        var newString
        if(retrievedList[i].restaurantID == 0){
           newString = `<div id = "restaurantCard" class = "eshankHomeCard"><div id="restaurantCardGrid"><div id = "restaurantThumbnailContainer"><img src="${retrievedList[i].thumbnailImage}" alt="Restaurant Thumbnail" id = "restaurantThumbnail"></div><div id = "restaurantCardData"><p id="restaurantCardMainText">${retrievedList[i].resturantName}</p><p id="restaurantCardSubText">${retrievedList[i].restaurantLocation}</p><img src="${ratingImage}" alt="5 Star Rating" id = "ratingStars"></div><div id = restaurantCardGridHeartIconContainer><img src="${retrievedList[i].isFavourite == true?"../themes/images/activatedHeartIcon.png":"../themes/images/deactivatedHeartIcon.png"}" onclick = "onFavouriteRestaurantIconClicked(${retrievedList[i].restaurantID});" alt="Favourite Icon" id = "heartIcon"></div></div></div>`
        }else{
           newString = `<div id = "restaurantCard"><div id="restaurantCardGrid"><div id = "restaurantThumbnailContainer"><img src="${retrievedList[i].thumbnailImage}" alt="Restaurant Thumbnail" id = "restaurantThumbnail"></div><div id = "restaurantCardData"><p id="restaurantCardMainText">${retrievedList[i].resturantName}</p><p id="restaurantCardSubText">${retrievedList[i].restaurantLocation}</p><img src="${ratingImage}" alt="5 Star Rating" id = "ratingStars"></div><div id = restaurantCardGridHeartIconContainer><img src="${retrievedList[i].isFavourite == true?"../themes/images/activatedHeartIcon.png":"../themes/images/deactivatedHeartIcon.png"}" onclick = "onFavouriteRestaurantIconClicked(${retrievedList[i].restaurantID});" alt="Favourite Icon" id = "heartIcon"></div></div></div>`
        }
        stringList = stringList + newString;
        }
    }
    if(stringList == ""){
        list.innerHTML = "<p id = 'emptyListText'>You're favourites are empty.</p>"
    }else{
        list.innerHTML = stringList;
    }
};

function onFavouriteRestaurantIconClicked(id) {
    var storedRestaurantList = JSON.parse(localStorage.getItem("restaurantList"));
    storedRestaurantList[id].isFavourite = false
    showSystemFeedbackPopup("removed");
    localStorage.setItem("restaurantList", JSON.stringify(storedRestaurantList));
    loadFavouriteRestaurantList(storedRestaurantList);
};

function onFavouriteIconClicked(id) {
    var storedRestaurantList = JSON.parse(localStorage.getItem("restaurantList"));
    if(storedRestaurantList[id].isFavourite){
        storedRestaurantList[id].isFavourite = false
        localStorage.setItem("restaurantList", JSON.stringify(storedRestaurantList));
        showSystemFeedbackPopup("removed");
    }else{
        storedRestaurantList[id].isFavourite = true
        showSystemFeedbackPopup("added");
        localStorage.setItem("restaurantList", JSON.stringify(storedRestaurantList));
    }
    var list = document.getElementById('restaurantList');
    var stringList = "";
    for (var i = 0;i < storedRestaurantList.length;i++) {
        var ratingImage;
        if(storedRestaurantList[i].rating == 5){
            ratingImage = "../themes/images/5starRating.png"
        }else{
            ratingImage = "../themes/images/4starRating.png"
        }
        var newString
        if(storedRestaurantList[i].restaurantID == 0){
            newString = `<div id = "restaurantCard" class = "eshankHomeCard"><div id="restaurantCardGrid"><div id = "restaurantThumbnailContainer"><img src="${storedRestaurantList[i].thumbnailImage}" alt="Restaurant Thumbnail" id = "restaurantThumbnail"></div><div id = "restaurantCardData"><p id="restaurantCardMainText">${storedRestaurantList[i].resturantName}</p><p id="restaurantCardSubText">${storedRestaurantList[i].restaurantLocation}</p><img src="${ratingImage}" alt="5 Star Rating" id = "ratingStars"></div><div id = restaurantCardGridHeartIconContainer><img src="${storedRestaurantList[i].isFavourite == true?"../themes/images/activatedHeartIcon.png":"../themes/images/deactivatedHeartIcon.png"}" onclick = "onFavouriteIconClicked(${storedRestaurantList[i].restaurantID});" alt="Favourite Icon" id = "heartIcon"></div></div></div>`
        }else{
            newString = `<div id = "restaurantCard"><div id="restaurantCardGrid"><div id = "restaurantThumbnailContainer"><img src="${storedRestaurantList[i].thumbnailImage}" alt="Restaurant Thumbnail" id = "restaurantThumbnail"></div><div id = "restaurantCardData"><p id="restaurantCardMainText">${storedRestaurantList[i].resturantName}</p><p id="restaurantCardSubText">${storedRestaurantList[i].restaurantLocation}</p><img src="${ratingImage}" alt="5 Star Rating" id = "ratingStars"></div><div id = restaurantCardGridHeartIconContainer><img src="${storedRestaurantList[i].isFavourite == true?"../themes/images/activatedHeartIcon.png":"../themes/images/deactivatedHeartIcon.png"}" onclick = "onFavouriteIconClicked(${storedRestaurantList[i].restaurantID});" alt="Favourite Icon" id = "heartIcon"></div></div></div>`
        }
        stringList = stringList + newString;
    }
    list.innerHTML = stringList;
};

function showSystemFeedbackPopup(status){
    if(status == "added"){
        $('.addedToFavouriteSysFeeedback').popup();
        $('.addedToFavouriteSysFeeedback').popup('open');
    }else{
        $('.removedFromFavouriteSysFeeedback').popup();
        $('.removedFromFavouriteSysFeeedback').popup('open');
    }
};
$(document).on('click', '.okBtn', function(){ 
    $('#systemFeedbackPopup').popup();
    $('#systemFeedbackPopup').popup('close');
    $('.emailSentPopup').popup();
    $('.emailSentPopup').popup('close');
    $('.removedFromFavouriteSysFeeedback').popup();
    $('.removedFromFavouriteSysFeeedback').popup('close');
    $('.addedToFavouriteSysFeeedback').popup();
    $('.addedToFavouriteSysFeeedback').popup('close');
    
});
$(document).on('click', '.cuisinesTabBarBtn', function(){ 
    $.mobile.changePage(".favCusPage")
});

$(document).on('click', '.shareEmailBtn', function(){ 
    $('.emailPopup').popup();
    $('.emailPopup').popup('open');
});

$(document).on('click', '#popupCloseIcon', function(){ 
    $('.emailPopup').popup();
    $('.emailPopup').popup('close');
});

$(document).on('click', '.sendEmailBtn', function(){ 
    var inputValue = document.getElementById("emailInputField").value
    if(inputValue){
        sendEmail(inputValue);
    }
});

function sendEmail(email){
    $('.emailPopup').popup();
    $('.emailPopup').popup('close');
    $('#emailInputField').val("");
    var rows = getHtmlTableRows();
    var html = '<p>Dear '+String(email)+',<br><br>Hereby, we have attached your favourite list of restaurants that you requested at urbaneats.com</p> <table style="border-collapse: separate;border-width:thin;width:100%;border-style: solid #183A1D;background-color: #FEFBE9;"> <tr> <th style="border:1px solid black;color: #183A1D;">Restaurant Name</th> <th style="border:1px solid black;color: #183A1D;">Location</th> <th style="border:1px solid black;color: #183A1D;">Delivery Time</th> <th style="border:1px solid black;color: #183A1D;">Rating</th> </tr>'+rows+'</table> <p>With Regards,<br>Team Urban Eats</p>'
    try{
        Email.send({
            Host: "smtp.gmail.com",
            Username: "testemailcoursework2017064@gmail.com",
            Password: "IIT2017064",
            To: email,
            From: "testemailcoursework2017064@gmail.com",
            Subject: "Favourites List from Urban Eats!",
            Body: html,
          })
            .then(function (message) {
                $('.emailSentPopup').popup();
                $('.emailSentPopup').popup('open');
            });
    }catch{
        $('.emailFailedPopup').popup();
        $('.emailFailedPopup').popup('open');
    }   
}

function getHtmlTableRows(){
    var retrievedList = JSON.parse(localStorage.getItem("restaurantList"));
    var tableRows = "";
    for (var i = 0;i < retrievedList.length;i++) {
        if(retrievedList[i].isFavourite){
            const myArray = retrievedList[i].restaurantLocation.split("  |  ", 2);
            var restaurantLocation = myArray[0];
            var deliveryTime = myArray[1];
            var rows = ' <tr><td style="border:1px solid black;color: #183A1D; text-align: center;">'+retrievedList[i].resturantName+'</td><td style="border:1px solid black;color: #183A1D; text-align: center;">'+restaurantLocation+'</td><td style="border:1px solid black;color: #183A1D; text-align: center;" >'+deliveryTime+'</td><td style="border:1px solid black;color: #183A1D; text-align: center;">'+String(retrievedList[i].rating)+' Stars</td></tr>'
            tableRows = tableRows+rows;
        }
    }
    return tableRows;
}

$(document).on('click', '.tabBarCuisinesBtn', function(){ 
    var cuisinesTabBar = document.getElementsByClassName("cuisinesTabBar")[0]
    var restaurantsTabBar = document.getElementsByClassName("restaurantsTabBar")[0]
    cuisinesTabBar.innerHTML = '<div id = "yellowLineTabBar"></div>'
    restaurantsTabBar.innerHTML = ''
    var list = document.getElementById('favouriteRestaurantList');
    var stringList = "";
    for (var i = 0;i < favCuisinesList.length;i++) {
        var newString = `<div id = "restaurantCard"><div id="restaurantCardGrid"><div id = "restaurantThumbnailContainer"><img src="${favCuisinesList[i].thumbnailImage}" alt="Restaurant Thumbnail" id = "restaurantThumbnail"></div><div id = "restaurantCardData"><p id="restaurantCardMainText">${favCuisinesList[i].cuisineName}</p><p id="restaurantCardSubText">${favCuisinesList[i].price}</p><p id="restaurantCardMainText">${favCuisinesList[i].resturantName}</p></div><div id = restaurantCardGridHeartIconContainer><img src="${favCuisinesList[i].isFavourite == true?"../themes/images/activatedHeartIcon.png":"../themes/images/deactivatedHeartIcon.png"}" alt="Favourite Icon" id = "heartIcon"></div></div></div>`
        stringList = stringList + newString;
    }
    list.innerHTML = stringList;
});

$(document).on('click', '.tabBarChangeRestaurantBtn', function(){ 
    var cuisinesTabBar = document.getElementsByClassName("cuisinesTabBar")[0]
    var restaurantsTabBar = document.getElementsByClassName("restaurantsTabBar")[0]
    cuisinesTabBar.innerHTML = ''
    restaurantsTabBar.innerHTML = '<div id = "yellowLineTabBar"></div>'
    var storedRestaurantList = JSON.parse(localStorage.getItem("restaurantList"));
    if(storedRestaurantList == null){
        localStorage.setItem("restaurantList", JSON.stringify(categoryRestaurantList));
        loadFavouriteRestaurantList(categoryRestaurantList);
    }else{
        loadFavouriteRestaurantList(storedRestaurantList);
    }
});

$(document).on('click','.resturantFilterBtn',function(){
    $('.openResturantFilterPopup').popup();
    $('.openResturantFilterPopup').popup('open');
});