var commentsList = [
    {
        userName: "Jake Parker",
        profPic: "../themes/images/profile-pictures/jake-parker.png",
        message: "Best soup so far from Eshanka Home!",
        likes: 3,
        dislikes: 0,
        replies: [
            {
                userName: "Sharon Williams",
                profPic: "../themes/images/profile-pictures/sharon_williams.png",
                message: "Couldn't agree more.",
                likes: 2,
                dislikes: 0,
            }
        ]
    }
]


$(document).on("pageshow", function (event) {
    var page = event.target.getAttribute('key');
    if ( page == "itemPage") {
        var allComments = JSON.parse(localStorage.getItem("commentsList"));
        if (allComments == null){
            localStorage.setItem("commentsList", JSON.stringify(commentsList))
        }
        loadComments();
    }
});



const commentsContainer = document.getElementById("commentsSection");

var allComments = JSON.parse(localStorage.getItem("commentsList"));

function loadComments(){    
    allComments.forEach((comment, cmtId) => {
        const commentCard = document.createElement('div');
        commentCard.cardList = "comment-body";

        const commentCardContent =
        `
            <div class = "comment-card">
                <div class = "report-icon">
                    <a href="#reportCommentPopup" data-rel="popup" data-position-to="window" data-transition="pop">
                        <i class="material-icons-outlined">report_gmailerrorred</i>
                    </a>
                </div>                
                <img src="${comment.profPic}" class="comment-image">  
                <div class="comment-userName">
                    ${comment.userName} 
                </div>
                <div id="likes-dislikes-container" class="likes-dislikes-container">
                    <div class="likes">
                        <i onClick="likesToggle(this)" class="material-icons-outlined">thumb_up</i>               
                        <div class="no_of_likes">${comment.likes}</div>
                    </div>
                    <div class="dislikes">
                        <i onClick="disLikesToggle(this)" class="material-icons-outlined">thumb_down</i>
                        <div class="no_of_dislikes">${comment.dislikes}</div>
                    </div>
                </div>
                <div class="comment-body">
                    ${comment.message}
                </div>
                <div class="reply-button" onClick="toggleReplyInput()">
                    <i class="material-icons-outlined">reply</i>
                    <span>Reply</span>
                </div>                 
            </div>
            <input id="repliesInput" placeholder="Reply..." onkeydown="saveReply(this,${cmtId})"> 
            <div id="replies-container"></div>    

        `
        commentsContainer.innerHTML += commentCardContent;

        replyList = comment.replies;

        const repliesContainer = document.getElementById("replies-container");

        replyList.forEach((reply, repId) => {
            const replyCard = document.createElement('div');
            replyCard.cardList="reply-body";

            const replyCardContent =
            `
            <div class="dots-container">
                <div class=dot>
                </div>
                <div class=dot>
                </div>
                <div class=dot>
                </div>
            </div>
            <div class = "reply-card comment-card">
                <div class="report-icon">
                    <i class="material-icons-outlined">report_gmailerrorred</i>
                </div>            
                    <img src="${reply.profPic}" class="comment-image">           
                <div class="comment-userName">
                ${reply.userName} 
                </div>
                <div id="likes-dislikes-container" class="likes-dislikes-container">
                    <div class="likes">
                        <i id="likeIcon" onClick="addLike(this)" class="material-icons-outlined">thumb_up</i>               
                        <div class="no_of_likes">${reply.likes}</div>
                    </div>
                    <div class="dislikes">
                        <i id="disLikeIcon" onClick="addDislike(this)" class="material-icons-outlined">thumb_down</i>
                        <div class="no_of_dislikes">${reply.dislikes}</div>
                    </div>
                </div>
                <div class="comment-body">
                    ${reply.message}
                </div>
                <div class="reply-button">
                    <i class="material-icons-outlined">reply</i>
                    <span>Reply</span>
                </div>                
            </div>
            `
            repliesContainer.innerHTML += replyCardContent;
        })
    })
}
var jqElement = $('#commentsSection') ;
function saveComment(comment){
    if(event.key === 'Enter' && comment.value.trim() != '') {
        jqElement.empty();
        var newComment = {
            userName : "John Xina",
            message : comment.value,
            profPic: "../themes/images/profile-pictures/profile_image.png",
            likes: 0,
            dislikes: 0,
            replies: [] 
        }
        allComments.push(newComment);
        localStorage.setItem("commentsList", JSON.stringify(allComments))
        loadComments();
        $('#commentsInput').val('');
    }
}

function likesToggle(element){
    var likes = document.querySelector(".no_of_likes");
    const disLikeButtonClass = document.getElementById("disLikeIcon").className;
    if(element.classList == "material-icons-outlined"){
        if(disLikeButtonClass === 'material-icons-outlined'){
            element.classList.remove("material-icons-outlined");
            element.classList.add("material-icons");
            likes.textContent = parseInt(likes.textContent) + 1;
        }
    }else{
        element.classList.remove("material-icons");
        element.classList.add("material-icons-outlined");
        likes.textContent = parseInt(likes.textContent) - 1;
    }
}

function disLikesToggle(element){
    var dislikes = document.querySelector(".no_of_dislikes");
    const likeButtonClass = document.getElementById("likeIcon").className;
    if(element.classList == "material-icons-outlined"){
        if(likeButtonClass === 'material-icons-outlined'){
            element.classList.remove("material-icons-outlined");
            element.classList.add("material-icons");
            dislikes.textContent = parseInt(dislikes.textContent) + 1;
        }
    }else{
        element.classList.remove("material-icons");
        element.classList.add("material-icons-outlined");
        dislikes.textContent = parseInt(dislikes.textContent) - 1;
    }
}

function toggleReplyInput(){
    repliesInputTextField = document.getElementById("repliesInput");
    if (repliesInputTextField.style.display == "block"){
        repliesInputTextField.style.display = "none"
    }else{
        repliesInputTextField.style.display = "block";
    }
}

function saveReply(reply, commentID){
    if(event.key === 'Enter' && reply.value.trim() != '') {        
        jqElement.empty();
        var newReply = {
            userName : "John Xina",
            message : reply.value,
            profPic: "../themes/images/profile-pictures/profile_image.png",
            likes: 0,
            dislikes: 0
        }
        allComments[commentID].replies.push(newReply);
        localStorage.setItem("commentsList", JSON.stringify(allComments))
        loadComments();
    }
}