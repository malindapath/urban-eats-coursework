var cartItemList = [];

$(document).on("pageshow", function (event) {
    var page = event.target.getAttribute('key');
    if ( page == "cartPage") {
        onload();
        var itemList = JSON.parse(localStorage.getItem("cartItemList"));
        if(itemList == null){
            localStorage.setItem("cartItemList", JSON.stringify(cartItemList));
        }
        loadCartItemList();
    }
});

function loadCartItemList() {
    var list = document.getElementById('cartItemList');
    var stringList = "";
    var subTotal = 0;
    var totalUrbanPoints = 0;
    cartItemList = JSON.parse(localStorage.getItem("cartItemList"));
    for (var i = 0;i < cartItemList.length;i++) {
        var newString = ` <div id = "restaurantCard"> <div id="restaurantCardGrid"> <div id = "restaurantThumbnailContainer"> <img src="${cartItemList[i].thumbnailImage}" alt="Restaurant Thumbnail" id = "restaurantThumbnail"> </div> <div id = "restaurantCardData"> <p id="cardItemNameText">${cartItemList[i].itemName}</p> <p id="restaurantCardSubText"></p> <p id="cardItemPriceText">LKR ${String(cartItemList[i].itemPrice)}.00</p> </div> <div id = restaurantCardGridHeartIconContainer> <div id = "cardItemButtonContainer"> <i id = "addIconCartItem" class="material-icons" onclick = "onItemNegativeButtonClicked(${cartItemList[i].itemID});">remove_circle</i> <div id = "itemQtyCartContainer"><p id = "itemQtyCartText">${String(cartItemList[i].qty)}</p></div> <i onclick = "onItemPositiveButtonClicked(${cartItemList[i].itemID});" id = "addIconCartItem" class="material-icons">add_circle</i> </div> </div> </div> </div>`
        stringList = stringList + newString;
        subTotal =  subTotal + (cartItemList[i].itemPrice*cartItemList[i].qty);
        totalUrbanPoints = totalUrbanPoints + cartItemList[i].urbanPoints
    }
    total = subTotal + 50;
    $('.subTotalText').text("LKR "+String(subTotal)+".00");
    $('.deliveryChargeText').text("LKR 50.00");
    $('.totalText').text("LKR "+String(total)+".00");
    $('.urbanPointsCartTotal').text(String(totalUrbanPoints)+" UP");
    if(cartItemList.length == 0){
        var data = document.getElementById('cartPageData');
        data.innerHTML = "<p id = 'emptyListText'>You're cart is empty.</p>"
    }else{
        list.innerHTML = stringList;
    }
};

function onItemPositiveButtonClicked(id){
    var index = cartItemList.findIndex(value => value.itemID === id);
    var newQty = cartItemList[index].qty+1
    cartItemList[index].qty = newQty
    localStorage.setItem("cartItemList", JSON.stringify(cartItemList));
    loadCartItemList();
};
function onItemNegativeButtonClicked(id){
    var index = cartItemList.findIndex(value => value.itemID === id);
    var newQty = cartItemList[index].qty-1
    if(newQty <= 0){
        var filteredArray = cartItemList.filter(value => value.itemID > id);
        cartItemList = filteredArray;
    }else{
        cartItemList[index].qty = newQty
    }
    localStorage.setItem("cartItemList", JSON.stringify(cartItemList));
    loadCartItemList();
};


$(document).on('click', '#cartCheckoutBtn', function(){ 
    var cartItemList = null;
    localStorage.setItem("cartItemList", JSON.stringify(cartItemList));
    onCheckout();
});

$(document).on('click', '.infoBtn', function(){ 
    var closeBtn = document.getElementById("popupCloseIcon");
    closeBtn.style.display = "inline";
    $('.infoBtnPopup').popup();
    $('.infoBtnPopup').popup('open');
});

function onload(){
    var storedRestaurantList = JSON.parse(localStorage.getItem("location"));
    var address;
    if (storedRestaurantList == null){
        address = "Address not selected!";
    }
    else{
        address = storedRestaurantList[0].location;
    }
    var stuff = document.getElementById('getAddress');
    var newString = `<p id = "addressValueCart">${address}</p>`
    stuff.innerHTML = newString;
}

function onCheckout(){
    var checked =  $('input[name="radioBtn"]:checked').val();
    var str = $('.urbanPointsCartTotal').text();
    var accUrbanPoints = localStorage.getItem("accUrbanPoints");
    if (accUrbanPoints==null){
        localStorage.setItem("accUrbanPoints", str.replace("UP","").replace(" ",""));
    }
    else{
        var newAccUrbanPoints = parseInt(str)+parseInt(accUrbanPoints);
        localStorage.setItem("accUrbanPoints", newAccUrbanPoints.toString());
        
    }
    str = str.substring(0, str.length - 2);
    if(checked != undefined && cartItemList.length>0){
        if(checked == "card"){
            jQuery(function($) {
                var $form = $('#frmBooking');
                var handler = StripeCheckout.configure({
                  key: 'pk_test_cp21BcECf4kMMUbSlRlZlsMo',
                  token: function(token) {
                    if(token.id){
                        $('.paymentCardSuccessfullFeedback').popup();
                        $('.paymentCardSuccessfullFeedback').popup('open');
                        $('.urbanPointsDescPopup').text("You have recieved "+str+"UP from your order.");
                    }
                  }
                });
                  handler.open({
                    name: 'Pay '+$('.totalText').text()+' using',
                    currency: 'lkr',
                    description: "Checkout now to earn Urban Points!",
                    amount: $('.totalText').text()
                  });
                });
        }else{
            $('.paymentCashSuccessfullFeedback').popup();
            $('.paymentCashSuccessfullFeedback').popup('open');
            $('.urbanPointsDescPopup').text("You will recieve "+str+"UP from your order after delivery.");
            
        }
    }else{
        $('.selectPaymentMethodPopup').popup();
        $('.selectPaymentMethodPopup').popup('open');
    }
}

