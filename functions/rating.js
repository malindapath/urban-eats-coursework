var cartItemList = [
    {itemID:1213,thumbnailImage:"../themes/images/chickenSoupThumbnail.png", itemName:"Chicken Soup", itemPrice: 720, qty: 1, urbanPoints: 14, isThumbsup: 0},
    {itemID:3212,thumbnailImage:"../themes/images/nelumKoleThumbnail.png", itemName:"Chicken Burger", itemPrice: 620, qty: 1 , urbanPoints: 16, isThumbsup: 0}
]

$(document).on("pageshow", function (event) {
    var page = event.target.getAttribute('key');
    if ( page == "ratingPage") {
        const ratingStars = [...document.getElementsByClassName("star")];
        executeRating(ratingStars);
        loadCartItemList();
    }
});

function executeRating(stars) {
   const starClassActive = "star";
   const starClassUnactive = "star_border";
   const starClass = "material-icons star cream";
   const starsLength = stars.length;
   let i;
   stars.map((star) => {
      star.onclick = () => {
         i = stars.indexOf(star);
         j = stars.indexOf(star);
         if (star.className.indexOf(starClass) !== -1) {
            for (j; j < starsLength; ++j) {
                stars[j].innerHTML = starClassUnactive;
            }
            for (i; i >= 0; --i) {
                stars[i].innerHTML = starClassActive;
            }
         } else {
            for (i; i < starsLength; ++i) {
                stars[i].innerHTML = starClassUnactive;
            }
         }
      };
   });
};

function loadCartItemList() {
    var list = document.getElementById('cardList');
    var stringList = "";
    for (var i = 0;i < cartItemList.length;i++) {
        var thumbsUpIcon
        var thumbsDownIcon
        if (cartItemList[i].isThumbsup==1){
            thumbsUpIcon = "material-icons thumbup cream"
            thumbsDownIcon = "material-icons-outlined thumbDown cream"
        }else{
            thumbsDownIcon = "material-icons thumbDown cream"
            thumbsUpIcon = "material-icons-outlined thumbup cream"
        }
        var newString = ` <div id = "restaurantCard"> <div id="restaurantCardGrid"> <div id = "restaurantThumbnailContainer"> <img src="${cartItemList[i].thumbnailImage}" alt="Restaurant Thumbnail" id = "restaurantThumbnail"> </div> <div id = "restaurantCardData"> <p id="cardItemNameText">${cartItemList[i].itemName}</p><p id="restaurantCardSubText"></p> <p id="cardItemPriceText">LKR ${String(cartItemList[i].itemPrice)}<i onclick="changeColorUp(${cartItemList[i].itemID})" class = "${thumbsUpIcon}" id = "thumb">thumb_up</i><i onclick="changeColorDown(${cartItemList[i].itemID})" class = "${thumbsDownIcon}" id = "thumbDown">thumb_down</i></p> </div> </div> </div>`
        stringList = stringList + newString;
    }
    list.innerHTML = stringList;
    
};

function changeColorUp(id) {
    var index = cartItemList.findIndex(value => value.itemID === id);
    cartItemList[index].isThumbsup=1;
    loadCartItemList();
}
function changeColorDown(id) {
    var index = cartItemList.findIndex(value => value.itemID === id);
    cartItemList[index].isThumbsup=0;
    loadCartItemList();
}

$(document).on('click', '#smallYellowBtn', function(){ 
    var closeBtn = document.getElementById("popupCloseIcon");
    closeBtn.style.display = "inline";
    $('.emailSentPopup').popup();
    $('.emailSentPopup').popup('open');
});
