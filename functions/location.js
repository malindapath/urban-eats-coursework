var locationList = [
    {location:"ex"}
]

var longitud;
var latitud;

var options = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
};
  
function success(pos) {
    var crd = pos.coords;
    latitud = crd.latitude;
    longitud = crd.longitude;

    map = new google.maps.Map(document.getElementById("googleMap"), {
        center: {lat: latitud, lng: longitud},
        zoom: 14
    });
};
  
function error(err) {
    document.getElementById("googleMap").innerHTML = ('ERROR(' + err.code + '): ' + err.message);
};
  
function myMap(){
navigator.geolocation.getCurrentPosition(success, error);
}

$(document).on('click','.getLocationButton',function(){
    getLocation()
    $('.emailSentPopup').popup();
    $('.emailSentPopup').popup('open');
});

$(document).on('click', '#popupCloseIcon', function(){ 
    $('.emailPopup').popup();
    $('.emailPopup').popup('close');
});

function getLocation(){
    var setLocation = document.getElementById("urbanPoints").innerHTML
    locationList[0].location = setLocation;
    var location = JSON.parse(localStorage.getItem("location"));
    if(location == null){
        localStorage.setItem("location", JSON.stringify(locationList));
    }
    else
        location.push(locationList);
}
