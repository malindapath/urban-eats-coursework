var itemList = [
    {itemID:Math.floor(1000 + Math.random() * 9000),thumbnailImage:"../themes/images/chickenSoupThumbnail.png", mainImage:"../themes/images/chickenSoup.png" , itemName:"Chicken Soup", itemPrice: 720, qty: 1, urbanPoints: 15,  restaurantName : "Eshanka Home", itemDescription:"Chicken soup is made from chicken, simmered in water, with various other ingredients. Our chicken soup consists of a clear chicken broth, with pieces of chicken and vegetables; additions are pasta, noodles, dumplings, or grains such as rice and barley. "},
    {itemID:Math.floor(1000 + Math.random() * 9000),thumbnailImage:"../themes/images/homelyFoodThumbnail.png", mainImage:"../themes/images/riceCurryHomely.png" , itemName:"Egg Rice and Curry", itemPrice: 170, qty: 1, urbanPoints: 10, restaurantName : "Homely Food", itemDescription:"Egg rice and curry is one of the most popular Chinese dishes. It's a classic stir-fry that usually includes soya sauce, onions and eggs, along with various other vegetables. Similar stir fry dishes are very popular not only in China, but in all South Asia and India as well!"},
    {itemID:Math.floor(1000 + Math.random() * 9000),thumbnailImage:"../themes/images/homelyFoodThumbnail.png", mainImage:"../themes/images/toastBreadPaan.png" , itemName:"Toast Bread", itemPrice: 250, qty: 1, urbanPoints: 5, restaurantName : "Home Paan", itemDescription:"Toast Bread is made with eggs, bread, onions, chilies and herbs. Egg toast is one of the most popular street foods from India. It is very much eaten in all regions as a breakfast or fast food."},
    {itemID:Math.floor(1000 + Math.random() * 9000),thumbnailImage:"../themes/images/biteMeThumbnail.png", mainImage:"../themes/images/burgerBiteMe.png" , itemName:"Chicken Burger", itemPrice: 750, qty: 1, urbanPoints: 20, restaurantName : "Bite Me", itemDescription:"Chicken Burger is packed with greek inspired flavors and is perfectly grilled and delicious. Wonderfully seasoned and it makes a great addition to any family weeknight meal. Low-carb and gluten free."},
]

$(document).on("pageshow", function (event) {
    var page = event.target.getAttribute('key');
    if ( page == "itemPage") {
        let currentPageListIndex;
        let searchParams = new URLSearchParams(window.location.search)
        if(searchParams.has('index')){
            currentPageListIndex = parseInt(searchParams.get('index'))
        }else{
            currentPageListIndex = 0
        }
        onload(currentPageListIndex)
    }
});
function onload(currentPageListIndex){
    var sub = document.getElementById('itemPoiCard');
    var tot = itemList[currentPageListIndex].itemPrice
    var newString = `<p id="screenHeading"><div id= "iconTitleContainer"><p id="screenHeading">${itemList[currentPageListIndex].itemName}</p><img src="../themes/images/deactivatedHeartIcon.png"  alt="Favourite Icon" id = "heartIcon"></div><p id="urbanPoints">${itemList[currentPageListIndex].urbanPoints} UP (Urban Points) <i class="material-icons info celtic" id="infoIcon">info</i></p><P id="resName">${itemList[currentPageListIndex].restaurantName}</P><img src="${itemList[currentPageListIndex].mainImage}" alt="Chicken Soup Thumbnail" id = "itemPoiImage"> <p id="itemDescription">${itemList[currentPageListIndex].itemDescription}</p><div id = "cardItemButtonContainer"> <i id = "addIconCartItem" class="material-icons" onclick = "onItemNegativeButtonClicked(${currentPageListIndex});">remove_circle</i> <div id = "itemQtyCartContainer"><p id = "itemQtyCartText">${itemList[currentPageListIndex].qty}</p></div> <i onclick = "onItemPositiveButtonClicked(${currentPageListIndex});" id = "addIconCartItem" class="material-icons">add_circle</i> </div> </div><div id = "addToCartBtn"> <p class = "subTotalText" id = "cardTotalSectionSmallTxtRight"></p> <p id = "addToCartBtnText">Add to cart</p> <i class="material-icons add_shopping_cart celtic">add_shopping_cart</i> </div>`
    sub.innerHTML = newString;
    $('.subTotalText').text("LKR " + String(tot)+".00");
};
function onItemPositiveButtonClicked(currentPageListIndex){
    var newQty = itemList[0].qty+1
    itemList[0].qty = newQty
    onload(currentPageListIndex);
};
function onItemNegativeButtonClicked(currentPageListIndex){
    var newQty = itemList[0].qty
    if(newQty <= 1){
        newQty = 1
        itemList[0].qty = newQty
        onload(currentPageListIndex);
    }else{
        newQty = itemList[0].qty-1
        itemList[0].qty = newQty
        onload(currentPageListIndex);
    }
};
$(document).on('click','#addToCartBtn',function(){
    var closeBtn = document.getElementById("popupCloseIcon");
    closeBtn.style.display = "inline";
    addToCart();
    $('.emailSentPopup').popup();
    $('.emailSentPopup').popup('open');
});
function addToCart(){
    var oldItemList = JSON.parse(localStorage.getItem("cartItemList"));
    if(oldItemList == null || oldItemList == []){
        localStorage.setItem("cartItemList", JSON.stringify(itemList));
    }
    else{
        let currentPageListIndex;
        let searchParams = new URLSearchParams(window.location.search)
        if(searchParams.has('index')){
            currentPageListIndex = parseInt(searchParams.get('index'))
        }else{
            currentPageListIndex = 0
        }
        oldItemList.push(itemList[currentPageListIndex]);
        localStorage.setItem("cartItemList", JSON.stringify(oldItemList));
    }   
}
$(document).on('click','#infoIcon',function(){
    var closeBtn = document.getElementById("popupCloseIcon");
    closeBtn.style.display = "inline";
    $('.urbanPointPopup').popup();
    $('.urbanPointPopup').popup('open');
});