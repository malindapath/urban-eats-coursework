var soupItemList = [
    {restaurantID:0,thumbnailImage:"../themes/images/chickenSoupThumbnail.png", itemName:"Chicken Soup", itemPrice: 720, isFavourite:false, type: "chicken"},
    {restaurantID:1,thumbnailImage:"../themes/images/vegSoup.png", itemName:"Vegetable Soup", itemPrice: 620, isFavourite:false, type: "vegetarian"}
]

var riceItemList = [
    {restaurantID:0,thumbnailImage:"../themes/images/homelyFoodThumbnail.png", itemName:"Chicken Rice and Curry", itemPrice: 220, isFavourite:false, type: "chicken"},
    {restaurantID:1,thumbnailImage:"../themes/images/eggRice.png", itemName:"Egg Rice and Curry", itemPrice: 170, isFavourite:false, type: "egg"}
]

$(document).on("pageshow", function (event) {
    var page = event.target.getAttribute('key');
    if ( page == "restaurantPage") {
        var storedSoupItemList = JSON.parse(localStorage.getItem("soupItemList"));
        var storedRiceItemList = JSON.parse(localStorage.getItem("riceItemList"));
        if(storedSoupItemList == null || storedRiceItemList == null){
            localStorage.setItem("soupItemList", JSON.stringify(soupItemList));
            localStorage.setItem("riceItemList", JSON.stringify(riceItemList));
            loadSoupItemList(soupItemList);
            loadRiceItemList(riceItemList);
        }else{
            loadSoupItemList(storedSoupItemList);
            loadRiceItemList(storedRiceItemList);
        }
    }
});

function loadSoupItemList(retrievedList) {
    var list = document.getElementById('soupItemList');
    var stringList = "";
    for (var i = 0;i < retrievedList.length;i++) {
        var newString = `<a href="../pages/itemPoi.html?index=${i}" rel="external"><div id = "restaurantCard"><div id="restaurantCardGrid"><div id = "restaurantThumbnailContainer"><img src="${retrievedList[i].thumbnailImage}" alt="Restaurant Thumbnail" id = "restaurantThumbnail"></div><div id = "restaurantCardData"><p id="restaurantCardMainText">${retrievedList[i].itemName}</p><p id="restaurantCardSubText">LKR ${String(retrievedList[i].itemPrice)}.00</p></div><div id = restaurantCardGridHeartIconContainer><img src="${retrievedList[i].isFavourite == true?"../themes/images/activatedHeartIcon.png":"../themes/images/deactivatedHeartIcon.png"}"  alt="Favourite Icon" id = "heartIcon"></div></div></div></a>`
        stringList = stringList + newString;
    }
    list.innerHTML = stringList;
};

function loadRiceItemList(retrievedList) {
    var list = document.getElementById('riceItemList');
    var stringList = "";
    for (var i = 0;i < retrievedList.length;i++) {
        var newString = `<div id = "restaurantCard"><div id="restaurantCardGrid"><div id = "restaurantThumbnailContainer"><img src="${retrievedList[i].thumbnailImage}" alt="Restaurant Thumbnail" id = "restaurantThumbnail"></div><div id = "restaurantCardData"><p id="restaurantCardMainText">${retrievedList[i].itemName}</p><p id="restaurantCardSubText">LKR ${String(retrievedList[i].itemPrice)}.00</p></div><div id = restaurantCardGridHeartIconContainer><img src="${retrievedList[i].isFavourite == true?"../themes/images/activatedHeartIcon.png":"../themes/images/deactivatedHeartIcon.png"}"  alt="Favourite Icon" id = "heartIcon"></div></div></div>`
        stringList = stringList + newString;
    }
    list.innerHTML = stringList;
};

$(document).on('click', '.infoBtn', function(){ 
    var closeBtn = document.getElementById("popupCloseIcon");
    closeBtn.style.display = "inline";
    $('.emailSentPopup').popup();
    $('.emailSentPopup').popup('open');
});

$(document).on('click', '.videoBtn', function(){ 
    var closeBtn = document.getElementById("popupCloseIcon");
    closeBtn.style.display = "inline";
    $('.videoPopUp').popup();
    $('.videoPopUp').popup('open');
});

$(document).on('click', '.beefBtn', function(){ 
    var id_filter = [0,1];
    var filteredSoup = soupItemList.filter(function(item){
        return id_filter.indexOf(item.restaurantID) !==-1 && item.type==="beef";
    })
    var filteredRice = riceItemList.filter(function(item){
        return id_filter.indexOf(item.restaurantID) !==-1 && item.type==="beef";
    })
    loadSoupItemList(filteredSoup)
    loadRiceItemList(filteredRice)
});

$(document).on('click', '.veganBtn', function(){ 
    var id_filter = [0,1];
    var filteredSoup = soupItemList.filter(function(item){
        return id_filter.indexOf(item.restaurantID) !==-1 && item.type==="vegan";
    })
    var filteredRice = riceItemList.filter(function(item){
        return id_filter.indexOf(item.restaurantID) !==-1 && item.type==="vegan";
    })
    loadSoupItemList(filteredSoup)
    loadRiceItemList(filteredRice)
});

$(document).on('click', '.chickenBtn', function(){ 
    var id_filter = [0,1];
    var filteredSoup = soupItemList.filter(function(item){
        return id_filter.indexOf(item.restaurantID) !==-1 && item.type==="chicken";
    })
    var filteredRice = riceItemList.filter(function(item){
        return id_filter.indexOf(item.restaurantID) !==-1 && item.type==="chicken";
    })
    loadSoupItemList(filteredSoup)
    loadRiceItemList(filteredRice)
});

$(document).on('click', '.750Btn', function(){ 
    var id_filter = [0,1];
    var filteredSoup = soupItemList.filter(function(item){
        return id_filter.indexOf(item.restaurantID) !==-1 && item.itemPrice<750;
    })
    var filteredRice = riceItemList.filter(function(item){
        return id_filter.indexOf(item.restaurantID) !==-1 && item.itemPrice<750;
    })
    loadSoupItemList(filteredSoup)
    loadRiceItemList(filteredRice)
});

$(document).on('click', '.1000Btn', function(){ 
    var id_filter = [0,1];
    var filteredSoup = soupItemList.filter(function(item){
        return id_filter.indexOf(item.restaurantID) !==-1 && item.itemPrice>1000;
    })
    var filteredRice = riceItemList.filter(function(item){
        return id_filter.indexOf(item.restaurantID) !==-1 && item.itemPrice>1000;
    })
    loadSoupItemList(filteredSoup)
    loadRiceItemList(filteredRice)
});

$(document).on('click', '.returantFilterBtnIcon', function(){ 
    $('.filterPopUp').popup();
    $('.filterPopUp').popup('open');
});

$(document).on('click', '.applyBtn', function(){
    var val1 = $('input[name=type]:checked').val();
    var val2 = $('input[name=price]:checked').val();

    var id_filter1 = [0,1];
    var filtered1 = soupItemList.filter(function(item){
        if (val2 > 1000){
            return id_filter1.indexOf(item.restaurantID) !==-1 && item.itemPrice>val2 && item.type===val1;
        }
        else if(val2 < 1000){
            return id_filter1.indexOf(item.restaurantID) !==-1 && item.itemPrice<val2 && item.type===val1;
        }
        else if(val2 < 750){
            return id_filter1.indexOf(item.restaurantID) !==-1 && item.itemPrice<val2 && item.type===val1;
        }
        else if(val2 < 500){
            return id_filter1.indexOf(item.restaurantID) !==-1 && item.itemPrice<val2 && item.type===val1;
        }
    })
    loadSoupItemList(filtered1)

    var id_filter2 = [0,1];
    var filtered2 = riceItemList.filter(function(item){
        if (val2 > 1000){
            return id_filter2.indexOf(item.restaurantID) !==-1 && item.itemPrice>val2 && item.type===val1;
        }
        else if(val2 < 1000){
            return id_filter2.indexOf(item.restaurantID) !==-1 && item.itemPrice<val2 && item.type===val1;
        }
        else if(val2 < 750){
            return id_filter2.indexOf(item.restaurantID) !==-1 && item.itemPrice<val2 && item.type===val1;
        }
        else if(val2 < 500){
            return id_filter2.indexOf(item.restaurantID) !==-1 && item.itemPrice<val2 && item.type===val1;
        }
    })
    loadRiceItemList(filtered2)
    
});

// $(document).on('click', '.applyBtn', function(){
//     var val1 = $('input[name=type]:checked').val();

//     var id_filter = [0,1];
//     var filteredSoup = soupItemList.filter(function(item){
//         return id_filter.indexOf(item.restaurantID) !==-1 && item.type===val1;
//     })
//     var filteredRice = riceItemList.filter(function(item){
//         return id_filter.indexOf(item.restaurantID) !==-1 && item.type===val1;
//     })
//     loadSoupItemList(filteredSoup)
//     loadRiceItemList(filteredRice)
// });

// $(document).on('click', '.applyBtn', function(){
//     var val1 = $('input[name=type]:checked').val();
//     var val2 = $('input[name=price]:checked').val();

//     var id_filter1 = [0,1];
//     var filtered1 = soupItemList.filter(function(item){
//         if (val2 > 1000){
//             return id_filter1.indexOf(item.restaurantID) !==-1 && item.itemPrice>val2;
//         }
//         else if(val2 < 1000){
//             return id_filter1.indexOf(item.restaurantID) !==-1 && item.itemPrice<val2;
//         }
//         else if(val2 < 750){
//             return id_filter1.indexOf(item.restaurantID) !==-1 && item.itemPrice<val2;
//         }
//         else if(val2 < 500){
//             return id_filter1.indexOf(item.restaurantID) !==-1 && item.itemPrice<val2;
//         }
//     })
//     loadSoupItemList(filtered1)

//     var id_filter2 = [0,1];
//     var filtered2 = riceItemList.filter(function(item){
//         if (val2 > 1000){
//             return id_filter2.indexOf(item.restaurantID) !==-1 && item.itemPrice>val2;
//         }
//         else if(val2 < 1000){
//             return id_filter2.indexOf(item.restaurantID) !==-1 && item.itemPrice<val2;
//         }
//         else if(val2 < 750){
//             return id_filter2.indexOf(item.restaurantID) !==-1 && item.itemPrice<val2;
//         }
//         else if(val2 < 500){
//             return id_filter2.indexOf(item.restaurantID) !==-1 && item.itemPrice<val2;
//         }
//     })
//     loadRiceItemList(filtered2)
// });

$(document).on('click','.applyBtn',function(){
    $('.filterPopUp').popup();
    $('.filterPopUp').popup('close');
});

$(document).on('click', '.filterCancelBtn', function(){ 
    soupItemList = soupItemList;
    riceItemList = riceItemList;
    loadSoupItemList(soupItemList)
    loadRiceItemList(riceItemList)
});

$(document).on('click','.filterCancelBtn',function(){
    $('.filterPopUp').popup();
    $('.filterPopUp').popup('close');
});

