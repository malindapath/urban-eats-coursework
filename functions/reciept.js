var cartItemList = [];

$(document).on("pageshow", function (event) {
    var page = event.target.getAttribute('key');
    if ( page == "receiptPage") {
        onload();
        var itemList = JSON.parse(localStorage.getItem("cartItemList"));
        if(itemList == null){
            localStorage.setItem("cartItemList", JSON.stringify(cartItemList));
        }
        loadCartItemList();
    }
});
function loadCartItemList() {
    var list = document.getElementById('cartItemList');
    var stringList = "";
    var subTotal = 0;
    var totalUrbanPoints = 0;
    cartItemList = JSON.parse(localStorage.getItem("cartItemList"));
    for (var i = 0;i < cartItemList.length;i++) {
        var newString = ` <div id = "restaurantCard"><p id = "cardItemQtyText">${String(cartItemList[i].qty)} Items</p> <div id="restaurantCardGrid"> <div id = "restaurantThumbnailContainer"> <img src="${cartItemList[i].thumbnailImage}" alt="Restaurant Thumbnail" id = "restaurantThumbnail"> </div> <div id = "restaurantCardData"> <p id="cardItemNameText">${cartItemList[i].itemName}</p> <p id="restaurantCardSubText"></p> <p id="cardItemPriceText">LKR ${String(cartItemList[i].itemPrice)}.00</p><div id = restaurantCardGridHeartIconContainer></div> </div> </div> </div>`
        stringList = stringList + newString;
        subTotal =  subTotal + (cartItemList[i].itemPrice*cartItemList[i].qty);
        totalUrbanPoints = totalUrbanPoints + cartItemList[i].urbanPoints
    }
    total = subTotal + 50;
    $('.subTotalText').text("LKR "+String(subTotal)+".00");
    $('.deliveryChargeText').text("LKR 50.00");
    $('.totalText').text("LKR "+String(total)+".00");
    $('.urbanPointsCartTotal').text(String(totalUrbanPoints)+" UP");
    if(cartItemList.length == 0){
        var data = document.getElementById('cartPageData');
        data.innerHTML = "<p id = 'emptyListText'>You're cart is empty.</p>"
    }else{
        list.innerHTML = stringList;
    }
};

function onload(){
    var storedRestaurantList = JSON.parse(localStorage.getItem("location"));
    var address;
    if (storedRestaurantList == null){
        address = "Address not selected!";
    }
    else{
        address = storedRestaurantList[0].location;
    }
    var stuff = document.getElementById('getAddress');
    var newString = `<p id = "addressValueCart">${address}</p>`
    stuff.innerHTML = newString;
}

$(document).on('click', '.infoBtn', function(){ 
    var closeBtn = document.getElementById("popupCloseIcon");
    closeBtn.style.display = "inline";
    $('.emailSentPopup').popup();
    $('.emailSentPopup').popup('open');
});